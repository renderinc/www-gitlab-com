---
layout: markdown_page
title: "FY20-Q3 OKRs"
---

This fiscal quarter will run from August 1, 2019 to October 31, 2019.

## On this page
{:.no_toc}

- TOC
{:toc}

### 1. CEO: IACV.

1.

### 2. CEO: Popular next generation product.

1. VP of Product Strategy: Get strategic thinking into the org. 3 [strategy reviews](https://gitlab.com/gitlab-com/Product/issues/379) for key industry categories (project management, application security testing, CD/RA).

### 3. CEO: Great team.

1.
